const express = require('express')
const cors = require('cors')
const mongoose = require('mongoose')
require('dotenv').config()
const articles = require('./routes/articles')
const tutos = require('./routes/tutos')
const maps = require('./routes/maps')
const InitiateMongoServer = require('./config/db')
const tokenValidator = require('./middleware/auth')
const errorMiddleware = require('./middleware/error')
const app = express()

// Init MongoServer
InitiateMongoServer()

// PORT
const port = process.env.PORT || 5000

// Middleware
app.use(cors())
app.use(express.json())
app.use(tokenValidator)

app.get('/', (req, res) => {
  res.send('welcom')
})

app.use('/articles', articles)
app.use('/tutos', tutos)
app.use('/maps', maps)

app.use(errorMiddleware)

const server = app.listen(port, () => {
  console.log(`Data API listening at http;//localhost:${port}`)
})

// Graceful shutdown
function handleGracefullShutdown (sig) {
  console.info(`${sig} signal received.`)
  console.log('Closing http server...')
  server.close(() => {
    console.log('Http server closed.')
    mongoose.connection.close(false, () => {
      console.log('MongoDb connection closed.')
      process.exit(0)
    })
  })
}

process.on('SIGINT', handleGracefullShutdown)
process.on('SIGTERM', handleGracefullShutdown)
process.on('SIGHUP', handleGracefullShutdown)
