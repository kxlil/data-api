const mongoose = require('mongoose')

const MapSchema = mongoose.Schema({
  title: {
    type: String,
    required: true,
    unique: true
  },
  code: {
    type: String,
    required: true,
    unique: true
  },
  imageUrl: {
    type: String,
    required: true
  },
  tags: [{
    type: String,
    require: true
  }],
  likes: [{
    type: String,
    require: true
  }],
  description: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now()
  }
})

module.exports = mongoose.model('map', MapSchema)
