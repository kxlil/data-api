const mongoose = require('mongoose')

const ArticleSchema = mongoose.Schema({
  slug: {
    type: String,
    required: true,
    unique: true
  },
  comments: {
    type: [{
      body: { type: String, required: [true, 'a body is requied'] },
      date: { type: Date, default: Date.now },
      author: { type: String, required: [true, 'an author is required'] }
    }]
  }
})

module.exports = mongoose.model('article', ArticleSchema)
