const mongoose = require('mongoose')

const TutoSchema = mongoose.Schema({
  slug: {
    type: String,
    required: true,
    unique: true
  },
  comments: {
    type: [{
      body: { type: String, required: true },
      date: { type: Date, default: Date.now },
      author: { type: String, reqired: true }
    }]
  }
})

module.exports = mongoose.model('tuto', TutoSchema)
