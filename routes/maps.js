const express = require('express')
const router = express.Router()
const pagination = require('../middleware/pagination')
const adminCheck = require('../middleware/admin')

// Mongoose Model
const Map = require('../model/Map')

router.get('/', pagination(Map), async (req, res) => {
  req.paginatedResults.docs.forEach(el => {
    el.like = {
      number: el.likes.length,
      liked: el.likes.includes(req.user.id)
    }
    delete el.likes
  })

  res.status(200).json(req.paginatedResults)
})

router.post('/', adminCheck, async (req, res, next) => {
  if (!req.user.admin) {
    return res.sendStatus(403)
  }
  const {
    title,
    code,
    imageUrl,
    tags,
    description
  } = req.body

  try {
    let map = new Map({
      title,
      code,
      imageUrl,
      tags,
      description
    })

    map = await map.save()
    // Turn mongoose document into plain js Object
    map = map.toObject()

    map.like = {
      number: map.likes.length,
      liked: map.likes.includes(req.user.id)
    }
    delete map.likes

    res.status(201).json({
      map
    })
  } catch (err) {
    next(err)
  }
})

router.post('/:id/likes', async (req, res, next) => {
  const id = req.params.id

  const query = { _id: id }
  let update = {
    $addToSet: { likes: req.user.id }
  }

  try {
    const { nModified } = await Map.updateOne(query, update)
    let liked = true

    if (!nModified) {
      update = {
        $pull: { likes: req.user.id }
      }

      await Map.updateOne(query, update)
      liked = false
    }

    res.status(200).json({
      liked
    })
  } catch (err) {
    next(err)
  }
})

router.delete('/:id', adminCheck, async (req, res, next) => {
  const id = req.params.id

  if (!req.user.admin) {
    return res.sendStatus(403)
  }

  try {
    const map = await Map.findByIdAndDelete(id)
    if (!map) {
      return res.sendStatus(404)
    }
    res.sendStatus(204)
  } catch (err) {
    next(err)
  }
})

module.exports = router
