const express = require('express')
const router = express.Router()

// Mongoose Model
const Tuto = require('../model/Tuto')

router.get('/:slug/comments', async (req, res, next) => {
  const slug = req.params.slug

  try {
    let tuto = await Tuto.findOne({
      slug
    })

    if (!tuto) {
      tuto = new Tuto({
        slug
      })

      await tuto.save()

      return res.status(201).json({
        comments: tuto.comments
      })
    }

    // Sort comments by date
    const comments = tuto.comments.sort((a, b) => {
      return b.date - a.date
    })

    res.status(200).json({
      comments
    })
  } catch (err) {
    next(err)
  }
})

router.post('/:slug/comments', async (req, res, next) => {
  const {
    body,
    author
  } = req.body

  const slug = req.params.slug

  const query = { slug }
  const update = {
    $push: { comments: { body, author } }
  }
  const options = { runValidators: true, new: true }

  try {
    const tuto = await Tuto.findOneAndUpdate(query, update, options)
    if (!tuto) {
      return res.status(400).json({
        message: 'The article you are trying to comment on does not exist'
      })
    }

    res.status(200).json({
      comment: tuto.comments.pop()
    })
  } catch (err) {
    next(err)
  }
})

module.exports = router
