const express = require('express')
const router = express.Router()

// Mongoose Model
const Article = require('../model/Article')

router.get('/:slug/comments', async (req, res, next) => {
  const slug = req.params.slug

  try {
    let article = await Article.findOne({
      slug
    })

    if (!article) {
      article = new Article({
        slug
      })

      await article.save()

      return res.status(201).json({
        comments: article.comments
      })
    }

    // sort comments by date
    const comments = article.comments.sort((a, b) => {
      return b.date - a.date
    })

    res.status(200).json({
      comments
    })
  } catch (err) {
    next(err)
  }
})

router.post('/:slug/comments', async (req, res, next) => {
  const {
    body,
    author
  } = req.body

  const slug = req.params.slug

  const query = { slug }
  const update = {
    $push: { comments: { body, author } }
  }
  const options = { runValidators: true, new: true }

  try {
    const article = await Article.findOneAndUpdate(query, update, options)
    if (!article) {
      return res.status(400).json({
        message: 'The article you are trying to comment on does not exist'
      })
    }

    res.status(200).json({
      comment: article.comments.pop()
    })
  } catch (err) {
    next(err)
  }
})

// router.post('/create', async (req, res) => {
//   try {
//     const {
//       slug,
//       comments
//     } = req.body

//     const article = new Article({
//       slug,
//       comments
//     })

//     await article.save()

//     res.send(article)
//   } catch (err) {
//     console.log(err)
//     res.status(500).send(err)
//   }
// })

module.exports = router
