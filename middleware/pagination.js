module.exports = (model) => {
  return async (req, res, next) => {
    let page, limit

    if (req.query.limit && req.query.page) {
      page = parseInt(req.query.page)
      limit = parseInt(req.query.limit)
    } else {
      page = 1
      limit = 5
    }

    const startIndex = (page - 1) * limit
    const endIndex = page * limit

    const pagination = {}

    if (endIndex < await model.countDocuments()) {
      pagination.next = {
        page: page + 1,
        limit: limit
      }
    }

    try {
      pagination.docs = await model.find().sort({ date: -1 }).limit(limit).skip(startIndex).lean()
      req.paginatedResults = pagination
      next()
    } catch (err) {
      res.status(500).json({
        message: err.message
      })
    }
  }
}
