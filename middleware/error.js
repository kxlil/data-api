const handleDuplicateKeyError = (err, res) => {
  const field = Object.keys(err.keyValue)
  const code = 409
  const errors = [{ message: `This ${field} is already used` }]
  res.status(code).json({ errors })
}

const handleValidationError = (err, res) => {
  const errors = Object.values(err.errors).map(el => { return { message: el.message } })
  const code = 400
  res.status(code).json({ errors })
}

module.exports = (err, req, res, next) => {
  try {
    if (err.name === 'ValidationError') return handleValidationError(err, res)
    if (err.code && err.code === 11000) return handleDuplicateKeyError(err, res)
    throw new Error('unknown error')
  } catch (err) {
    res.status(500).send('An unknown error occurred')
  }
}
