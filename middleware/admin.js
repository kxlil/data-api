const jwt = require('jsonwebtoken')
const axios = require('axios')

module.exports = async (req, res, next) => {
  try {
    const payload = {
      user: {
        id: req.user.id
      }
    }
    let Authorization = 'Bearer'

    await jwt.sign(
      payload,
      process.env.ACCESS_TOKEN_SECRET,
      { expiresIn: 1200 },
      (err, token) => {
        if (err) throw err
        Authorization = `${Authorization} ${token}`
      }
    )

    const data = await axios.get(`${process.env.AUTH_BASE_URL}/users/me`, { headers: { Authorization } })
    req.user.admin = data.data.user.admin
    next()
  } catch (err) {
    console.log(err.message)
    next(err)
  }
}
